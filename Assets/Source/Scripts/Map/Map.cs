﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class MapRules
{
    public int theme = 0;
    public int entrances = 1;
    public int maxRow = 4;
}

[System.Serializable]
public class MapTheme
{
    public GameObject startNodePrefab;
    public GameObject endNodePrefab;

    public List < GameObject > walkablePrefabs;
    public List < GameObject > nonWalkablePrefabs;

    public List < GameObject > waterPrefabs;
    public List < GameObject > bridgePrefabs;

    public List < GameObject > towerSpawnPrefabs;
}

public struct Point
{
    public int row;
    public int column;

    public Point(int row, int column)
    {
        this.row = row;
        this.column = column;
    }
}

public class Map : MonoBehaviour
{
    public const int NonWalkableTile = 1;

    public const int StartTile = 2;
    public const int EndTile = 3;
    public const int WalkTile = 4;
    public const int WaterTile = 5;
    public const int BridgeTile = 6;
    public const int TowerTile = 7;

    public MapRules rules;
    public List < MapTheme > themes;

    public int[,] grid = new int[20, 11]
    {
        { 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0 },
        { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
        { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
        { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
        { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
        { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
        { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
        { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
        { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
        { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
        { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
        { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
        { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
        { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
        { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
        { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
        { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
        { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
        { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
        { 0, 0, 0, 0, 3, 3, 3, 0, 0, 0, 0 },

    };

    public IEnumerator LoadMap()
    {
        MapTheme theme = themes[rules.theme];
        int maxRows = grid.GetUpperBound(0);
        int maxColumns = grid.GetUpperBound(1);

        // generate the entrances 
        GenerateEntrances(maxRows, maxColumns);

        // generate the water

        // generate the roads

        // intantiate all prefabs

        yield return null;

    }

    public void GenerateEntrances(int maxRows, int maxColumns)
    {
        List < Point > validEntrances = new List < Point >();

        // get the top of the map
        for (int column = 0; column < maxColumns; column++)
        {
            if (grid[0, column] != 0)
                validEntrances.Add(new Point(0, column));   
        }

        // get the sides of the grid until the rules tell me to stop
        for (int row = 0; row < rules.maxRow; row++)
        {
            if (grid[row, 0] != 0)
                validEntrances.Add(new Point(row, 0));

            if (grid[row, maxColumns] != 0)
                validEntrances.Add(new Point(row, maxColumns));
        }

        List < Point > entrances = new List < Point >();
        // decide what all the start nodes are

        for (int i = 0; i < rules.entrances; i++)
        {
            entrances.Add(validEntrances[Random.Range(0, validEntrances.Count)]);
        }

        foreach (Point p in entrances)
        {
            grid[p.row, p.column] = StartTile; 
        }
    }
}
