﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Unit
{
    public string Name;
    public UnitType unitType;
    public int cost;
    public int count;
    public int damage;
    public float hitSpeed;
    public float speed;
    public float deployTime;
    public float lifeTime;
    public float range;
    public TargetType targetType;


}

public enum UnitType
{
    Troop,
    Building,
    Spell
}

[System.Flags]
public enum TargetType
{
    Building = 1 << 1,
    Air = 1 << 2,
    Ground = 1 << 3,
}

public enum Team
{
    Player,
    Enemy
}

